function getIconImage(text, callback) {
    chrome.runtime.sendMessage({text: text}, callback);
}

function getAllIconKeys(callback) {
    chrome.runtime.sendMessage({keys: true}, callback);
}

(function($) {
    var $set = getElements();
    var interval = 5 * 1000;
    var time = (new Date()).getTime();
    var modified = false;

    function getElements() {
        return $('body *:not(script):not(code):not(:has(> *)):visible');
    }

    function replaceIconTextByImage(text) {
        if(document.getElementsByTagName('body')[0].innerText.indexOf(text) < 0) return;
        
        var found = $set.filter(function(k,v) {
            return v.innerText.indexOf(text) > -1;
        });

        if(found.length < 1) return;

        console.log("Found " + text);

        getIconImage(text, function(response) {
            if(!response || !response[text]) return;

            var image = response[text];

            found.each(function(key, val) {
                var $val = $(val);
                $val.html($val.text().replace(text, "<img src='" + image + "' title='" + text + "' alt='" + text + "' style='margin:0;padding:0;border:0' />"));
            });
        });
    }

    function replaceIcons() {
        getAllIconKeys(function(response) {
            response.forEach(function(val) {
                replaceIconTextByImage(val);
            });
        });
    }



    var hasListener = $('body').attr('data-hc-icons-everywhere-listener');
    if(hasListener !== "true") {
        $('body').attr('data-hc-icons-everywhere-listener', "true");
        $('body').on('DOMSubtreeModified', function() {modified = true;});
        setInterval(function(){
            if(modified) {
                $set = getElements();
                replaceIcons();
                modified = false;
            }
        }, 5000);
    }

    replaceIcons();

})(jQuery);
