function execJQueryScript(callback) {
    chrome.tabs.executeScript({
        file: "libs/jquery-2.1.4.min.js",
    }, callback);
}

function execIconScript(callback) {
    chrome.tabs.executeScript({
        file: "icons.js"
    }, callback);
}

function execScripts(callback) {
    execJQueryScript(execIconScript.bind(null, callback));
}

function shouldStop(tab) {
    return tab.url.substr(0,9) === "chrome://";
}

function clearDB() {
    chrome.storage.local.clear(function() {
      if(chrome.runtime.lastError) {
        console.error(
          "Error clearing database" +
          ": " + chrome.runtime.lastError.message
        );
      }
    });
}

function populateDB() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', chrome.extension.getURL('icons.json'), true);
    xhr.onreadystatechange = function() {
        if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            var icons = JSON.parse(xhr.responseText);

            icons.forEach(function(val) {
                addDBRecord(val.text, val.icon);
            });
        }
    };
    xhr.send();
}

function addDBRecord(key, data) {
    var obj = {};
    obj[key] = data;

    chrome.storage.local.set(obj, function() {
      if(chrome.runtime.lastError) {
        console.error(
          "Error setting " + key + " to " + JSON.stringify(data) +
          ": " + chrome.runtime.lastError.message
        );
      }
    });
}

function getDBRecord(key, callback) {
    chrome.storage.local.get(key, callback);
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if(request.text) {
        getDBRecord(request.text, sendResponse);
    }
    else if(request.keys) {
        getDBRecord(null, function(result) {
            sendResponse(Object.keys(result));
        });
    }

    return true; // return true, so the channel knows we send a response asynchronously.
});

chrome.runtime.onInstalled.addListener(function(details){
    if(details.reason == "install"){
        console.log("This is a first install!");
    } else if(details.reason == "update"){
        var thisVersion = chrome.runtime.getManifest().version;
        console.log("Updated from " + details.previousVersion + " to " + thisVersion + "!");
    }

    clearDB();
    populateDB();
});

chrome.tabs.onCreated.addListener(function(tab) {
    if(shouldStop(tab)) return;

    execScripts(function() {
        console.log("done onCreated");
    });
});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if(shouldStop(tab)) return;

    if(changeInfo.status === "complete") {
        execScripts(function() {
            console.log("done onUpdated");
        });
    }
});
