var fs = require('fs');
var path = require('path');
var https = require('https');
var jsdom = require("jsdom").jsdom;
var jquery = require('jquery');
var opts = require("nomnom").options({
    path: {
        position: 0,
        help: "Output file",
        required: true
    },
    verbose: {
        abbr: 'v',
        help: "Verbose output",
        flag: true
    },
    silent: {
        abbr: 's',
        help: "No output",
        flag: true
    }
}).parse();

var wstream = fs.createWriteStream(path.resolve(opts.path));
var url = 'https://www.hipchat.com/emoticons';

function writeIconArrayToJSONStream($icons, $) {
    wstream.write("[");

    $.each($icons, function(idx, val) {
        var $val = $(val);

        wstream.write((idx === 0 ? "" : ",\n") + JSON.stringify({
            text: $val.data('clipboard-text'),
            icon: $val.find('img').attr('src')
        }));
    });

    wstream.write("]");
}

wstream.on('open', function() {
    https.get(url, function(res) {
        if(opts.verbose && !opts.silent) {
            console.log(res.req.method + " " + url);
            console.log("Got response: " + res.statusCode + " " + res.statusMessage);
        }

        var content = "";

        res.on('data', function(chunk) {
            content += chunk;
        });

        res.on('end', function() {
            var window = jsdom(content).parentWindow;
            var $ = jquery(window);

            var $icons = $('.emoticon-block');

            writeIconArrayToJSONStream($icons, $);

            wstream.on('finish', function() {
                window.close();
                if(!opts.silent) console.log("Wrote " + $icons.length + " icons to " + opts.path);
            });

            wstream.end();
        });
    }).on('error', function(e) {
        console.log("Got error: " + e.message);
    });
});
